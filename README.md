# Zammad

Ansible role to install Zammad on Debian using [debs](https://docs.zammad.org/en/latest/install/debian.html) or [source](https://docs.zammad.org/en/latest/install/source.html).

See also the Ansible to install a [Zammad development server](https://git.coop/webarch/zammad-server).
